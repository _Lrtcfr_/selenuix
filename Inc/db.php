<?php
	// Inclusion des fichiers
	include 'connexion.inc.php';

	$HOSTNAME = HOSTNAME;
	$DB_SELECTED = DB_SELECTED;
	$USERNAME = USERNAME;
	$PASSWORD = PASSWORD;

	try {
	    $db = new PDO("mysql:host=$HOSTNAME;dbname=$DB_SELECTED", $USERNAME, $PASSWORD);    
	    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	} catch (PDOException $e) {
	    print "Erreur ! " . $e->getMessage() . "<br/>";
	    die();
	}
?>
