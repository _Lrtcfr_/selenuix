<!-- Navbar -->
		<nav class="navbar navbar-transparent navbar-fixed-top navbar-color-on-scroll">
			<div class="container">
		        <div class="navbar-header">
			    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index">
			        	<span class="sr-only">Toggle navigation</span>
			        	<span class="icon-bar"></span>
			        	<span class="icon-bar"></span>
			        	<span class="icon-bar"></span>
			    	</button>
			    	<a href="http://lrtcfr.servehttp.com/">
			        	<div class="logo-container">
			                <div class="logo">
			                    <img src="assets/img/logo.png" alt="Lrtcfr Logo" rel="tooltip" title="<b>Sélénuix</b> est un site d'actualité, de <i>Sélénuix</i>" data-placement="bottom" data-html="true">
			                </div>
			                <div class="brand">
			                    Sélénuix
			                </div>
						</div>
			      	</a>
			    </div>
			    <div class="collapse navbar-collapse" id="navigation-index">
			    	<ul class="nav navbar-nav navbar-right">
				    	<li>
							<a href="news.php">Actualités</a>
						</li>						
						<li>
							<a href="formations.php">Formations</a>
						</li>						
						<!--<li>
							<a href="profil.php">Profil</a>
						</li>-->											
						<li>
							<a href="register.php">Inscription</a>
						</li>
						<li>
							<a href="login.php">Connexion</a>
						</li>
			    	</ul>
			    </div>
			</div>
		</nav>
		<!-- End Navbar -->
		
<?php if (isset($_SESSION['flash'])): ?>
	<?php foreach ($_SESSION['flash'] as $type => $message): ?>
		<div class="alert alert-<?= $type; ?>">
			<?= $message; ?>
		</div>
	<?php endforeach; ?>
	<?php unset($_SESSION['flash']); ?>
<?php endif; ?>