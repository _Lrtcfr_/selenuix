		<footer class="footer">
			<div class="container">
				<nav class="pull-left">
				    <ul>
						<li>
							<a href="http://lrtcfr.servehttp.com/">Sélénuix</a>
						</li>
						<li>
							<a href="about.php">About Us</a>
						</li>
						<li>
							<a href="contact.php">Contact</a>	
						</li>
						<li>
							<a rel="tooltip" title="Suivez nous sur Twitter" data-placement="top" href="https://twitter.com/selenuix" target="_blank" class="btn btn-white btn-simple btn-just-icon">
								<i class="fa fa-twitter"></i>
							</a>
						</li>
						<li>
							<a rel="tooltip" title="Suivez nous sur Instagram" data-placement="top" href="https://www.instagram.com/anthony_ciszek/" target="_blank" class="btn btn-white btn-simple btn-just-icon">
								<i class="fa fa-instagram"></i>
							</a>
						</li>	
				    </ul>
				</nav>
				<div class="copyright pull-right">
					<p>&copy; 2016 Sélénuix </p>
				</div>
			</div>
		</footer>
	</body>
</html>