	<?php require 'Inc/header.php'; ?>
	<title>Accueil</title>		
</head>
<body class="index-page">
	<?php require_once 'Inc/navbar.php'; ?> 
		<div class="wrapper">
			<div class="header header-filter" style="background-image: url('assets/img/bg2.jpeg');">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="brand">
								<h1>Sélénuix</h1>
								<h3>Suivez toute l'actualité Geek, Linux, Open-Source et Développement</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="main main-raised">
				<div class="section section-basic">
			    	<div class="container">
			            <div class="title">
			                <h2 align="center">Derniers Articles :</h2>
			            </div>
			            <div class="row">		            	
							<div class="panel panel-primary">
	  							<div class="panel-heading">
	    							<h3 class="panel-title" align="center">Catégorie / Sous-Catégorie : Titre</h3>
	  							</div>
	  							<div class="panel-body" align="center">
	    							Preview
	  							</div>
							</div>
			            </div>
			            <div class="title">
			            	<h2 align="center">Dernières activités sur le forum :</h2>
			            	<div class="row">
			            		<div class="panel panel-primary">
	  								<div class="panel-heading">
	    								<h3 class="panel-title" align="center">Forum</h3>
	  								</div>
	  								<div class="panel-body" align="center">
	    								Preview
	  								</div>
			            		</div>
			           		</div>
			           	</div>
			        </div>
			    </div>
			</div>
		</div>
	<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js" type="text/javascript"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="assets/js/material-kit.js" type="text/javascript"></script>

	<script type="text/javascript">
		$().ready(function(){
			// the body of this function is in assets/material-kit.js
			materialKit.initSliders();
            window_width = $(window).width();

            if (window_width >= 992){
                big_image = $('.wrapper > .header');
				$(window).on('scroll', materialKitDemo.checkScrollForParallax);
			}
		});
	</script>
		<?php require 'Inc/footer.php'; ?>
</body>