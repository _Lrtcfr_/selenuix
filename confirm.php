<?php 
	$user_id = $_GET['id'];
	$token = $_GET['token'];

	require_once 'Inc/db.php';
	$req = $db->prepare('SELECT * FROM users WHERE id = ?');
	$req->execute([$user_id]);
	$user = $req->fetch();

	if ($user && $user->confirmation_token == $token) {
		session_start();
		$db->prepare('UPDATE users SET confirmation_token = NULL, confirmed_at = NOW()')->execute([$user]);
		$_SESSION['flash']['success'] = "Votre compte à bien été validé";
		$_SESSION['auth'] = $user_id;
		header('location: account.php');
	} else {
		$_SESSION['flash']['danger'] = "Ce lien à éxpiré, ou n'est pas valide !";
		header('location: login.php');
	}
?>