<?php
	// Inclusion des fichiers	
	require_once 'Inc/autoload.php';
	require_once 'Inc/header.php';

	if (!empty($_POST)) {
		$errors = array(); // Tableau d'erreur

		// Test validité nom d'utilisateur
		if (empty($_POST['username']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['username'])) {
			$errors['invalid_username'] = "Votre nom d'utilisateur n'est pas valide !";
		} else {
			$req = $db->prepare('SELECT id FROM users WHERE username = ?');
			$req->execute([$_POST['username']]);
			$user = $req->fetch();
			if ($user) {
				$errors['username_taken'] = "Ce nom d'utilisateur est déjà utilisé !"; 
			}
		}

		// Test validité email
		if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$errors['invalid_email'] = "Votre adresse e-mail n'est pas valide !";		
		} else {
			$req = $db->prepare('SELECT id FROM users WHERE email = ?');
			$req->execute([$_POST['email']]);
			$email = $req->fetch();
		}

		// L'adresse email est-elle déjà prise ?		
		if ($email) {
			$errors['email_taken'] = "Cette adresse e-mail est déjà utilisé !";
		}		

		// Test confirmation mot de passe
		if (empty($_POST['password']) || $_POST['password'] =! $_POST['password_confirm']) {
			$errors['password'] = "Veuillez entrer un mot de passe valide !";
		}

		// S'il n'y a pas d'erreurs
		if (empty($errors)) {		
			$req = $db->prepare('INSERT INTO users SET username = ?, password = ?, email = ?, confirmation_token = ?');
			$password = password_hash($_POST['password'], PASSWORD_BCRYPT);
			$token = str_random(60);
			$req->execute([$_POST['username'], $password, $_POST['email'], $token]);
			$user_id = $db->lastInsertId();
			
			// Envoi du mail
			$message = Swift_Message::newInstance('Confirmez votre compte');
			$message->setFrom(['lrtcfr@gmail.com']);
			$message->setBody("<p>Veuiller <a href='http://lrtcfr.servehttp.com/confirm.php?id=$user_id&token=$token'>confirmez</a> votre compte Sélénuix.</p>", 'text/html');

			$message->setTo([$_POST['email']]);
			$message->addPart("<p>Veuiller confirmer votre compte Sélénuix. Visiter ce lien pour l'activer : http://lrtcfr.servehttp.com/confirm.php?id=user_id&token=$token</p>", 'text/plain');

			$transport = Swift_SmtpTransport::newInstance('localhost', 1025);
			$mailer = Swift_Mailer::newInstance($transport);
			$mailer->send($message);
			$_SESSSION['flash']['success'] = "Un e-mail de confirmation vous à été envoyé, veuillez vérifier vos e-mails.";
			header('location: login.php');
			exit();	
		}
	}
?>
<!DOCTYPE html>
	<head>
		<title>Inscription</title>
		<?php require 'Inc/header.php'; ?>
	</head>		
	<body class="signup-page">
	<?php require_once 'Inc/navbar.php'; ?> 	
	    <div class="wrapper">
			<div class="header header-filter" style="background-image: url('assets/img/city.jpg'); background-size: cover; background-position: top center;">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
							<?php if(!empty($errors)): ?>
								<div class="form-group">
									<div class="alert alert-danger">
										<p>Votre formulaire contient des erreurs !</p>
											<ul>
												<?php foreach ($errors as $error): ?>
													<li><?= $error ?></li>
												<?php endforeach; ?>
											</ul>
									</div>
								</div>	
							<?php endif; ?>
							<div class="card card-signup">
								<form class="form" method="post" action="register.php">
									<div class="header header-primary text-center">
										<h4>Inscrivez-vous avec ...</h4>
										<div class="social-line">
											<a href="#pablo" class="btn btn-simple btn-just-icon">
												<i class="fa fa-facebook-square"></i>
											</a>
											<a href="#pablo" class="btn btn-simple btn-just-icon">
												<i class="fa fa-twitter"></i>
											</a>
											<a href="#pablo" class="btn btn-simple btn-just-icon">
												<i class="fa fa-google-plus"></i>
											</a>
										</div>
									</div>
									<p class="text-divider">Ou rester classique</p>
									<div class="content">
										<div class="input-group">
											<span class="input-group-addon">
												<i class="material-icons">face</i>
											</span>
											<input type="text" name="username" class="form-control" placeholder="Nom d'utilisateur" autofocus />
										</div>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="material-icons">email</i>
											</span>
											<input type="email" name="email" class="form-control" placeholder="Adresse e-mail"  />
										</div>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="material-icons">lock_outline</i>
											</span>
											<input type="password" name="password" placeholder="Mot de passe" class="form-control"  />
										</div>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="material-icons">lock_outline</i>
											</span>
											<input type="password" name="password_confirm" placeholder="Confirmation mot de passe" class="form-control"  />
										</div>
									</div>
									<div class="footer text-center">
										<button class="btn btn-primary"><a href="#pablo">S'inscrire</a></button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
	    </div>
<!--   Core JS Files   -->
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/material.min.js"></script>
	
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="assets/js/nouislider.min.js" type="text/javascript"></script>
	
<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>
	
<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="assets/js/material-kit.js" type="text/javascript"></script>

<!-- Footer -->
<?php require 'Inc/footer.php'; ?>